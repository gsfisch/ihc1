package com.example.tp2;

import androidx.appcompat.app.AppCompatActivity;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;
import android.content.Context;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager sensorManager;

    private Sensor luminositySensor;

    private Sensor pressureSensor;

    private Sensor positionSensor;

    TextView lightValue;

    TextView pressureValue;

    TextView positionValue;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lightValue = (TextView) findViewById(R.id.light);

        pressureValue = (TextView) findViewById(R.id.pressure);

        positionValue = (TextView) findViewById(R.id.position);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        luminositySensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

        pressureSensor = sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);

        if(luminositySensor != null)
        {
            sensorManager.registerListener(MainActivity.this, luminositySensor,
                    SensorManager.SENSOR_DELAY_NORMAL);
        }else
        {
            lightValue.setText("Light sensor not supported");
        }

        if(pressureSensor != null)
        {
            sensorManager.registerListener(MainActivity.this, pressureSensor,
                    SensorManager.SENSOR_DELAY_NORMAL);
        }else
        {
            pressureValue.setText("Pressure sensor not supported");
        }
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor sensor = sensorEvent.sensor;

        if(sensor.getType() == Sensor.TYPE_LIGHT)
        {
            lightValue.setText("Light Intensity: " + sensorEvent.values[0]);
        }

        else if(sensor.getType() == Sensor.TYPE_PRESSURE) {
            pressureValue.setText("Pressure Intensity: " + sensorEvent.values[0]);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}