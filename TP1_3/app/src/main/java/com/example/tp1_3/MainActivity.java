package com.example.tp1_3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;


public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager sensorManager;
    private EditText editTextX;
    private EditText editTextY;
    private EditText editTextZ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        editTextX = (EditText) findViewById(R.id.editTextX);
        editTextY = (EditText) findViewById(R.id.editTextY);
        editTextZ = (EditText) findViewById(R.id.editTextZ);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        boolean significantMove = false;

        try {
            if (Math.abs(sensorEvent.values[0] - Float.parseFloat(editTextX.getText().toString())) > 0.3 ||
                    Math.abs(sensorEvent.values[1] - Float.parseFloat(editTextY.getText().toString())) > 0.3 ||
                    Math.abs(sensorEvent.values[2] - Float.parseFloat(editTextZ.getText().toString())) > 0.3)
                significantMove = true;
        } catch (NumberFormatException e) { }

        editTextX.setText(String.valueOf(sensorEvent.values[0]));
        editTextY.setText(String.valueOf(sensorEvent.values[1]));
        editTextZ.setText(String.valueOf(sensorEvent.values[2]));

        if (significantMove) {

            Intent intent = new Intent(this, Activity2.class);
            startActivity(intent);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    protected void onResume() {
        super.onResume();

        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();

        sensorManager.unregisterListener(this);
    }
}