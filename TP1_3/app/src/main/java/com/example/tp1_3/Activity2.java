package com.example.tp1_3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {

    private TextView textView1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        textView1 = (TextView) findViewById(R.id.activity2_textView1);
    }

    public void returnFromActivity2(View v) {

        Intent intent = new Intent(this, MainActivity.class);

        startActivity(intent);
    }
}