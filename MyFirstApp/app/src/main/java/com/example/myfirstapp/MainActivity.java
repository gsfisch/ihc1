package com.example.myfirstapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void addValues(View v) {

        EditText editText1 = (EditText) findViewById(R.id.editText1);
        EditText editText2 = (EditText) findViewById(R.id.editText2);

        TextView textView1 = (TextView) findViewById(R.id.textView1);

        String text1 = editText1.getText().toString().trim();
        String text2 = editText2.getText().toString().trim();

        Double number1;
        Double number2;

        if(text1.length() == 0 || text1.length() == 0)
            return;

        try {
            number1 = Double.parseDouble(text1);
            number2 = Double.parseDouble(text2);
        } catch (NumberFormatException e) {

            textView1.setText("Invalid input(s)!");

            return;
        }

        textView1.setText(String.valueOf(number1 + number2));
    }
}