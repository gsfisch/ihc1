package com.example.tp_2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void launchActivity2(View v) {

        Intent intent = new Intent(this, Activity2.class);

        String text = ((EditText) findViewById(R.id.mainEditText1)).getText().toString();

        intent.putExtra("info", text);

        startActivity(intent);
    }
}